import pytest


@pytest.mark.asyncio
async def test_iter(hub):
    """
    Verify that when only stop iteration is put on the queue that it closes
    """
    await hub.ingress.init.publish(hub.ingress.init.STOP_ITERATION)

    # Verify that the event made it all the way through to being published
    async for _ in hub.ingress.init.iter():
        assert False, "This should be unreachable"


@pytest.mark.asyncio
async def test_publish(hub, evbus, event):
    await hub.ingress.init.publish(event)

    # Put a stop iteration event directly on the ingress queue so that it doesn't run forever
    await hub.ingress.init.publish(hub.ingress.init.STOP_ITERATION)

    # Verify that the event made it all the way through to being published
    async for result in hub.ingress.init.iter():
        assert result == event


@pytest.mark.asyncio
async def test_publishers(hub):
    contexts = {"init": None}
    ret = await hub.ingress.init.publishers(contexts)
    assert ret == [(hub.ingress.init.publish, None)]


@pytest.mark.asyncio
async def test_vomit(hub, evbus, event):
    await hub.ingress.init.vomit(event, [(hub.ingress.init.publish, None)])

    # Put a stop iteration event directly on the ingress queue so that it doesn't run forever
    await hub.ingress.init.publish(hub.ingress.init.STOP_ITERATION)

    # Verify that the event made it all the way through to being published
    async for result in hub.ingress.init.iter():
        assert result == event


@pytest.mark.asyncio
async def test_full(hub, evbus, event):
    """
    Test the path of an event through the bus
    The event fixture makes sure the event bus is running.
    Put an event directly on the event BUS so that it gets vomited onto all the ingress queues
    Verify that it made it all the way through to ingress
    """
    # Put an event on the queue
    await hub.evbus.BUS.put(event)

    # Put a stop iteration event directly on the ingress queue so that it doesn't run forever
    await hub.ingress.init.publish(hub.ingress.init.STOP_ITERATION)

    # Verify that the event made it all the way through to being published
    async for result in hub.ingress.init.iter():
        assert result == event
